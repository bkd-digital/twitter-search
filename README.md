This is a Twitter Search side project while I learn React, Redux, Jest & more of NodeJS.

# Twitter Search

### We're leveraging the following packages to connect & authenticate with Twitter:

- [Twitter oAuth](https://github.com/desmondmorris/node-twitter)

#### MacOS Dependencies
- Watchman <br />
```brew install watchman```

#### Project Dependencies
- ENV file (.env)
You'll need to create a new .env file at the root of the project and provide the following values:
CONSUMER_KEY=YOUR_VALUE_HERE_WITHOUT_QUOTES
CONSUMER_SECRET=YOUR_VALUE_HERE_WITHOUT_QUOTES
ACCESS_TOKEN_KEY=YOUR_VALUE_HERE_WITHOUT_QUOTES
ACCESS_TOKEN_SECRET=YOUR_VALUE_HERE_WITHOUT_QUOTES

PROXY_PORT=4000
LOCAL_HOST=http://localhost
