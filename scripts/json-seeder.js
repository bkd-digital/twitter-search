const destination = require('../config/paths').fakeData
const fs = require('fs')
const faker = require('faker')
const args = require('yargs').argv
const log = console.log

const usersCount = args.users || 1
let users = []

// Create a user object until we reach our userCount argument
for (let currentIndex = 0; currentIndex < usersCount; currentIndex++) {
  let
    firstName = faker.name.firstName(),
    lastName = faker.name.lastName(),
    address = {
      primary: {
        street: faker.address.streetAddress(),
        city: faker.address.city(),
        state: faker.address.stateAbbr(),
        zipcode: faker.address.zipCode()
      },
      mailing: {
        street: faker.address.streetAddress(),
        city: faker.address.city(),
        state: faker.address.stateAbbr(),
        zipcode: faker.address.zipCode()
      }
    },
    salary = Math.floor(Math.random() * (300000 - 24000 + 1)) + 24000,
    employment = faker.random.boolean()

  let user = {
    firstName,
    lastName,
    address,
    salary,
    employment
  }
  users.push(user)
}
users = JSON.stringify(users)
fs.writeFileSync(`${destination}/users.json`, users)
