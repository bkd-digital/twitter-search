import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { hydrateFeed, search, reset } from '../../store/statuses/statuses-actions'
import { keywords, hydrateKeywords } from '../../store/keywords/keywords-actions'
import Search from '../../components/search'

class Home extends Component {
  render () {
    return (
      <Fragment>
        <Search {...this.props} />
      </Fragment>
    )
  }
}

function mapStateToProps (state) {
  return {
    keywords: state.keywords.keywords,
    statuses: state.statuses.statuses,
    loader: state.loader.loader
  }
}

function mapDispatchToProps (dispatch) {
  return {
    handleChange: e => {
      const keywordValue = e.target.value ? e.target.value : ''
      keywords({ keywords: keywordValue }, dispatch)
    },
    handleSubmit: (e, keywords) => {
      e.preventDefault()
      search(keywords, dispatch)
    },
    hydrateApp: () => {
      hydrateFeed(dispatch)
      hydrateKeywords(dispatch)
    },
    resetSearch: e => {
      reset(dispatch)
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
