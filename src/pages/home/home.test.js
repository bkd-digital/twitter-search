import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import configureStore from 'redux-mock-store'
import React from 'react'

import Home from './home'

configure({ adapter: new Adapter() })

describe('Main Home page', () => {
  const mockStore = configureStore()
  let store, AppContainer

  const initialState = {
    keywords: [],
    statuses: [{
      id: '1',
      text: 'This is for tests only',
      user: {
        id: 23,
        profile_image_url_https: 'https://pbs.twimg.com/profile_images/941102968669200387/Vars95D2_400x400.jpg',
        description: 'test description'
      }
    }]
  }

  beforeEach(() => {
    store = mockStore(initialState)
    AppContainer = shallow(<Home store={store} />)
  })

  it('should be connected to our Redux store...', () => {
    console.log(AppContainer)
  })

  // it('expects the Search component', () => {
  //   expect(wrapper.find('form').exsists()).toBe(true)
  // })
})
