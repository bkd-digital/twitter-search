import styled from 'styled-components'
import { colors } from '../../styles/index'

export const TweetText = styled.p`
    width: 100%;
    margin: 0;
    margin-bottom: 20px;
    line-height: 150%;
    padding: 24px 20px;
    border-radius: 14px;
    box-shadow: -2px 2px 27px 0 rgba(3,3,3,.2);
    transition: background-color .2s ease-in-out;
    position: relative;

    &:hover {
      cursor: pointer;
      background-color: ${colors.blue};
      border-top-color: ${colors.blue};
      transition: background-color .2s ease-in-out;
      color: ${colors.white};
      
      &:after {
        border-top-color: ${colors.blue};
        transition: border-top-color .2s ease-in-out;
      }

      a {
       color: ${colors.green}
      }

    }

    &:after {
      content: ' ';
      position: absolute;
      transform: translateY(-50%);
      top: 35%;
      left: -6px;
      width: 0;
      height: 0;
      border: solid transparent;
      border-color: rgba(255, 255, 255, 0);
      border-top-color: #fff;
      border-width: 20px;
      margin-left: -25px;
      transform: rotate(83deg);
      transition: border-top-color .2s ease-in-out;
    }
    
`
