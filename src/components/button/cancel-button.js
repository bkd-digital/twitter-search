import React, { Component } from 'react'
import { string } from 'prop-types'
// figure out what's the best way to create styled components...this seems stupid
import ButtonSc from './styles'

export default class CancelButton extends Component {
  render () {
    return (
      <ButtonSc
        type={this.props.type}
        value={this.props.value}
      />
    )
  }
}

CancelButton.propTypes = {
  text: string,
  type: string
}

CancelButton.defaultProps = {
  text: 'Search...',
  type: 'submit'
}
