import styled from 'styled-components'
import {colors} from '../../styles'

const AnchorSc = styled.a`
  color: ${colors.blue};
  background-color: ${colors.lightBlue};
  display: inline-block;
  font-size: 12px;
  text-decoration: none;
  padding: 2px 4px;
  border-radius: 4px;
  border: 1px solid ${colors.black}
`

export default AnchorSc