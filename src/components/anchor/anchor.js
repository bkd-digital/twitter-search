import React, { Component } from 'react'
import AnchorSc from './styles'

class Anchor extends Component {
  render () {
    return (
      <AnchorSc
        href={this.props.href}
        title={this.props.title}
        target={this.props.target}>
        {this.props.copy}
      </AnchorSc>
    )
  }
}

export default Anchor
