import styled from 'styled-components'

const FormSc = styled.form`
  text-align: center;
  padding: 20px 0;
  h2 {
    padding: 10px 0;
  }
`
export default FormSc
