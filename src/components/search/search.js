import React, { Component } from 'react'
import PropTypes from 'prop-types'
import FormSc from './styles'
import { Button } from '../button'
import Input from '../input'
import TweetList from '../tweet-list'
import NoResults from '../no-results'
import Loader from '../loader'

class Search extends Component {
  componentWillMount () {
    this.props.hydrateApp()
  }

  render () {
    const statuses = this.props.statuses
    const keywords = this.props.keywords

    return (
      <React.Fragment>
        <FormSc
          onSubmit={(e) => {
            this.props.handleSubmit(e, keywords)
          }}
          compact='true'
        >
          <Input
            type='text'
            placeholder='Search user or hashtags'
            value={this.props.keywords || ''}
            onChange={this.props.handleChange}
          />
          <Button />
          <Button
            type='button'
            value='Clear'
            onClick={this.props.resetSearch}
          />
        </FormSc>
        {
          this.props.loader ? <Loader /> : null
        }
        {
          (statuses !== null && statuses.length > 0) ? <TweetList statuses={statuses} /> : <NoResults copy='Results will render here' />
        }
      </React.Fragment>

    )
  }
}

Search.proptypes = {
  statuses: PropTypes.array
}

export default Search
