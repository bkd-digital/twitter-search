import styled from 'styled-components'
import { colors } from '../../styles'

const FooterSC = styled.footer`
  text-align: center;
  font-size: 16px;
  background-color: ${colors.blue};
  color: ${colors.white};
  padding: 10px;
  ul {
    margin: 0;
    padding: 0;
    list-style-type: none;
    display: flex;
    justify-content: center
  }
  li {
    padding: 0 10px;
  }
`
export default FooterSC
