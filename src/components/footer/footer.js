import React, { Component } from 'react'
import { SocialIcon } from 'react-social-icons'
import FooterSC from './styles'

class Footer extends Component {
  render () {
    return (
      <FooterSC>
        <ul>
          <li>
            <SocialIcon network='twitter' url='//twitter.com/CavScoutVeteran' style={{ width: 30, height: 30 }} />
          </li>
          <li>
            <SocialIcon network='linkedin' url='//linkedin.com/in/cesarmperez' style={{ width: 30, height: 30 }} />
          </li>
        </ul>
      </FooterSC>
    )
  }
}

export default Footer
