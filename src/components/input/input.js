import React, { Component } from 'react'
import { string } from 'prop-types'
import InputSc from './styles'

export default class Input extends Component {
  constructor (props) {
    super()
  }
  render () {
    return (
      <InputSc
        type={this.props.type}
        value={this.props.value}
        {...this.props}
      />
    )
  }
}

Input.propTypes = {
  text: string,
  type: string
}

Input.defaultProps = {
  text: 'Search...',
  type: 'submit'
}
