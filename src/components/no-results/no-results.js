import React from 'react'
import styled from 'styled-components'

const H3 = styled.h3`
  text-align: center;
  font-size: 10px;
  color: #333;
`

const NoResults = (props) => {
  return (
    <H3>
      { props.copy }
    </H3>
  )
}

NoResults.defaultProps = {
  copy: 'Default no results message'
}

export default NoResults
