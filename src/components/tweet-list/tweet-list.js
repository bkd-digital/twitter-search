import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import Tweets from './styles'
import { TweetText } from '../tweet-text/tweet-text'
import ProfileImage from './user-profile-image'

class TweetList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      profileOpen: false
    }
  }

  parseTweets (text) {
    const words = text.split(' ')

    words.forEach((word, i) => {
      if (words[i].indexOf('@') !== -1) {
        // find usernames & create anchors to their profiles
        const pureUsername = words[i]
          .replace('@', '')
          .replace(':', '')

        // create anchors for usernames
        // need to clean up this heaping pile of crap below
        const userNameAnchor = `<a 
        rel='noopener noreferrer'
        target='_blank'
        href='//twitter.com/${pureUsername}'
        >
        @${pureUsername}
        </a>`
        words[i] = userNameAnchor
      } else if (words[i].indexOf('http') !== -1 || words[i].indexOf('https') !== -1) {
        // find urls and create anchors for them
        const url = words[i]
        const urlAnchor = `<a 
        rel='noopener noreferrer'
        target='_blank'
        href='${url}'
        >
        ${url}
        </a>`
        words[i] = urlAnchor
      }
    })

    return words.join(' ')
  }

  clickEvent (e) {
    this.setState({
      profileOpen: (this.state.profileOpen ? this.state.profileOpen : false)
    })
  }

  tweetItem = (tweet) => {
    const tweetID = tweet.id
    const timestamp = Math.round(new Date().getTime() / 1000)
    const text = {
      __html: this.parseTweets(tweet.text)
    }
    return (
      <li key={tweetID + timestamp}>
        <div>
          <ProfileImage
            clickEvent={this.clickEvent}
            userID={tweet.user.id}
            src={tweet.user.profile_image_url_https}
            alt={tweet.user.description}
          />
        </div>
        <TweetText dangerouslySetInnerHTML={text} />
      </li>
    )
  }

  buildList () {
    const statuses = this.props.statuses

    return (
      <Tweets>
        {statuses.map(tweet => {
          return this.tweetItem(tweet)
        })}
      </Tweets>
    )
  }

  render () {
    return (
      <React.Fragment>
        { this.buildList() }
      </React.Fragment>
    )
  }
}

TweetList.propTypes = {
  statuses: PropTypes.array
}

TweetList.defaultProps = {
  statuses: []
}

export default TweetList
