import styled from 'styled-components'
import { colors } from '../../styles'

const Tweets = styled.ul`
  width: 100%;
  margin: 0 auto;
  padding: 0;
  text-align: left;
  list-style-type: none;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  align-items: center;
  
  li {
    display: flex;
    width: 100%;
    padding: 0;
    
    &:last-child {
      margin-bottom: 0;
    }

    > div {
      margin-right: 20px;
      align-items: center;
      display: flex;
      max-width: 58px;
      width: 100%;
      img {
        border-radius: 50%;
        max-width: 100%;
      }
    }

  }
  a {
    color: ${colors.blue};
    text-decoration: none;
  }
`
export default Tweets
