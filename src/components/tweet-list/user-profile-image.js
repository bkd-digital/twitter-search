import React, { Component } from 'react'
import PropTypes from 'prop-types'

class ProfileImage extends Component {
  render() {
    return (
      <img
        onClick={this.props.clickEvent}
        userid={this.props.userID}
        src={this.props.src}
        alt={this.props.description}
      />
    )
  }
}

ProfileImage.propTypes = {
  userID: PropTypes.number,
  src: PropTypes.string,
  alt: PropTypes.string
}

ProfileImage.defaultProps = {
  userID: '',
  src: 'https://placeimg.com/50/50/animals/sepia',
  alt: 'Default alt text'
}

export default ProfileImage