import { configure, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import React from 'react'
import TweetList from './tweet-list'

configure({ adapter: new Adapter() });

const props = {
  statuses: [{
    id: '1',
    text: 'This is for tests only',
    user: {
      id: 23,
      profile_image_url_https: 'https://pbs.twimg.com/profile_images/941102968669200387/Vars95D2_400x400.jpg',
      description: 'test description'
    }
  }]
}

describe('Tweet List component', () => {
  let wrapper = mount(<TweetList {...props} />)

  it('Renders the list of tweets', () => {
    // console.log(wrapper.debug())
    // console.log(wrapper.find('li'))
    expect(wrapper.find('li').exists()).toBe(true)
  })

  it('Has a profile image', () => {
    // console.log(wrapper.debug())
    expect(wrapper.find('img').exists()).toBe(true)
  })

  it('Receives statuses from props', () => {
    expect(wrapper.prop('statuses'))
  })

})
