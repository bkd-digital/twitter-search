import React, { Component } from 'react'

class Navigation extends Component {
  render () {
    return (
      <nav>
        <ul>
          <li>Render Route Links here</li>
        </ul>
      </nav>
    )
  }
}

export default Navigation
