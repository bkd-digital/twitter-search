import React, { Component } from 'react'
import {string} from 'prop-types'
import HeaderSC from './styles'

class Header extends Component {
  render () {
    return (
      <HeaderSC>
        { this.props.text }
      </HeaderSC>
    )
  }
}

Header.propTypes = {
  text: string
}

Header.defaultProps = {
  text: 'Twitter Keyword Search'
}

export default Header
