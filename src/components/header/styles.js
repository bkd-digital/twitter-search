import styled from 'styled-components'
import { colors } from '../../styles/'

const HeaderSC = styled.header`
  text-align: center;
  font-size: 18px;
  font-weight: 700;
  background-color: ${colors.blue};
  color: ${colors.white};
  padding: 10px;
`
export default HeaderSC
