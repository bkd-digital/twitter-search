import LoaderSc from './styles'

import React, { Component } from 'react'

class Loader extends Component {
  render () {
    return (
      <LoaderSc />
    )
  }
}

export default Loader
