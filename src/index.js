import React, { Fragment } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { injectGlobal } from 'styled-components'
import storeConfig from './store'

// Import Master Layout
import Master from './layout/master'

// Import pages
import Home from './pages/home'
import Profile from './pages/profile'

import registerServiceWorker from './registerServiceWorker'

// Create our store
const store = storeConfig()

// Start listening to any subscriptions
// store.subscribe(() => {
//   console.log(store.getState())
// })

// Inject any global styling rules
injectGlobal`
  body {
    font-family: Calibri, Helvetica, Arial, sans-serif;
    font-size: 12px;
    padding: 0;
    margin: 0;
  }
`

const Root = ({ store }) => (
  <Master>
    <Provider store={store}>
      <Router>
        <Fragment>
          <Route exact path='/' component={Home} />
          <Route exact path='/profile' component={Profile} />
        </Fragment>
      </Router>
    </Provider>
  </Master>
)

ReactDOM.render(<Root store={store} />, document.getElementById('root'))
registerServiceWorker()
