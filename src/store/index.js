import { combineReducers, createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import keywords from './keywords/keywords-reducer'
import statuses from './statuses/statuses-reducer'
import loader from './loader-animation/loader-animation-reducer'

export default () => {
  const store = createStore(
    combineReducers({
      loader,
      keywords,
      statuses
    }),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(thunk)
  )
  return store
}
