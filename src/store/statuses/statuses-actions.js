import { actionTypes } from '../actionTypes'
import endpoints from '../../endpoints'
import { asyncLocalStorage } from '../../helpers/async-localStorage'
import fetch from 'cross-fetch'

export function search (keywords, dispatch) {
  keywords = keywords || ''

  let filterKeywords = keywords.replace('#', '') || ''

  dispatch({
    type: actionTypes.LOADER,
    loader: true
  })

  dispatch({
    type: actionTypes.GET_STATUSES,
    statuses: null
  })

  // Clear previously stored status from localStorage
  asyncLocalStorage
    .clear('statuses')
    .then(() => {
      // Request data
      fetch(endpoints.search + filterKeywords)
        .then(response => response.json())
        .then(data => {
          const statuses = data.statuses ? data.statuses : []

          // Persist statuses
          asyncLocalStorage
            .set('statuses', JSON.stringify(statuses))
            .then(() => {
              dispatch({
                type: actionTypes.LOADER,
                loader: true
              })
            })
            .then(() => {
              dispatch({
                type: actionTypes.GET_STATUSES,
                statuses
              })
            })
            .then(() => {
              dispatch({
                type: actionTypes.LOADER,
                loader: false
              })
            })
            .catch(function (error) {
              console.log(error)
            })
        })
        .catch((error) => {
          console.log(error)
        })
    })
}

export function hydrateFeed (dispatch) {
  // bring in our persisted state
  asyncLocalStorage
    .get('statuses')
    .then(res => {
      const results = JSON.parse(res)
      const statuses = results || null
      if (statuses) {
        dispatch({
          type: actionTypes.GET_STATUSES,
          statuses
        })
      }
    })
    .catch(function (error) {
      console.log(error)
    })
}

export function reset (dispatch) {
  // clear both keywords and statuses local storage entries
  asyncLocalStorage.clear('kewords')
  asyncLocalStorage
    .clear('statuses')
    .then(() => {
      dispatch({
        type: actionTypes.GET_STATUSES,
        statuses: null
      })
    })
    .catch(function (error) {
      console.log(error)
    })
}

export default { search, hydrateFeed, reset }
