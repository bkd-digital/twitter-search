import { actionTypes } from '../actionTypes'

const defaultState = {
  statuses: [],
  loader: false
}
const statuses = (state = defaultState, action) => {
  switch (action.type) {
    case actionTypes.GET_STATUSES:
      return {
        ...state,
        statuses: action.statuses
      }
    default:
      return state
  }
}

export default statuses
