import { actionTypes } from '../actionTypes'
import { asyncLocalStorage } from '../../helpers/async-localStorage'

export function keywords ({ keywords = '' } = {}, dispatch) {
  asyncLocalStorage.clear('keywords')
    .then(() => {
      asyncLocalStorage
        .set('keywords', JSON.stringify(keywords))
        .then(() => {
          dispatch({
            type: actionTypes.KEYWORDS,
            keywords
          })
        })
        .catch(error => {
          dispatch({
            type: actionTypes.ERROR,
            error
          })
        })
    })
}

export function hydrateKeywords (dispatch) {
  asyncLocalStorage.get('keywords')
    .then(res => {
      const localKeywords = JSON.parse(res)
      const keywords = localKeywords || null
      dispatch({
        type: actionTypes.KEYWORDS,
        keywords
      })
    })
}

export default {
  keywords,
  hydrateKeywords
}
