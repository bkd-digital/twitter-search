import { actionTypes } from '../actionTypes'

const defaultState = {
  keywords: ''
}

const keywords = (state = defaultState, action) => {
  switch (action.type) {
    case actionTypes.KEYWORDS:
      return {
        ...state,
        keywords: action.keywords,
        loader: true
      }
    case actionTypes.LOADER:
      return { ...state, loader: true }
    case actionTypes.RESET:
      return { ...state, loader: false, keywords: '' }
    default:
      return state
  }
}

export default keywords
