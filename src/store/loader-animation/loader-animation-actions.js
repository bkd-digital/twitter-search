import { actionTypes } from '../actionTypes'

export const loaderActions = {
  show: ({ loader = false } = {}, dispatch) => {
    dispatch({
      type: actionTypes.LOADER,
      loader
    })
  }
}
