import React, { Fragment } from 'react'
import styled from 'styled-components'

import Header from '../../components/header'
import Footer from '../../components/footer'

const Main = styled.main`
  padding: 20px;
`

const Master = props => ({
  render () {
    return (
      <Fragment>
        <Header />
        <Main>
          {this.props.children}
        </Main>
        <Footer />
      </Fragment>
    )
  }
})

export default Master
