export const asyncLocalStorage = {
  set: (key, value) => {
    return Promise.resolve().then(() => {
      return localStorage.setItem(key, value)
    })
  },
  clear: (key) => {
    return Promise.resolve().then(() => {
      return localStorage.removeItem(key)
    })
  },
  get: (key) => {
    return Promise.resolve().then(() => {
      return localStorage.getItem(key)
    })
  }
}