// load in environment variables
require('dotenv').load()
const fs = require('fs')
const myConfiguration = require('../config/paths')
const chalk = require('chalk')
const path = require('path')
const logNotify = chalk.bgKeyword('white').keyword('red')
const errorNotify = chalk.bgYellow.red
const env = process.env
const log = console.log
const Twitter = require('twitter')

// Bring in dummy / fake data
const usersData = require('../config/paths').fakeData + '/users.json'

const client = new Twitter({
  consumer_key: env.CONSUMER_KEY,
  consumer_secret: env.CONSUMER_SECRET,
  access_token_key: env.ACCESS_TOKEN_KEY,
  access_token_secret: env.ACCESS_TOKEN_SECRET
})

const express = require('express')
const app = express()

app.get('/', (req, res) => {
  res.sendFile(path.join(myConfiguration.appHtml))
})

app.get('/users', (req, res) => {
  const users = JSON.parse(
    fs.readFileSync(usersData, 'utf-8')
  )
  res.json(users)
})

app.get('/search', (req, res) => {
  client.get('search/tweets', {
    q: req.query.q,
    count: (req.query.count ? req.query.count : null),
    geocode: (req.query.geocode ? req.query.geocode : null)
  },
    (error, tweets, response) => {
      return res.send(tweets)
    }
  )
})

app.get('/user', (req, res) => {
  client.get('users/show', {
    user_id: req.query.id
  },
    (error, data, response) => {
      return res.send(data)
    }
  )
})

// Let's listen on the imported PORT env variable
app.listen(env.PROXY_PORT, () => console.log(`Server's started on port ${env.PROXY_PORT}`))

// Open a browser instance for convenience
// opn(`${env.LOCAL_HOST}:${env.PROXY_PORT}`, { app: 'google chrome' })

log(logNotify(`Let's get this party started...`))
